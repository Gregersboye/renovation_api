const Portal = require('./src/interfaces/AffaldsPortal');
const JsonGenerator = require('./src/Generators/Json');

exports.handler = async (event) => {
  const portal = new Portal();
  const data = await portal.getServices(event.pathParameters.addressId);
  return {
    statusCode: 200,
    body: new JsonGenerator(data),
  };

};
