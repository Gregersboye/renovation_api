const Portal = require('./src/interfaces/AffaldsPortal');

exports.handler = async (event) => {
  const searchTerm = decodeURI(event.pathParameters.address);
  const portal = new Portal();
  const data = await portal.getAddress(searchTerm);
  return {
    statusCode: 200,
    body: JSON.stringify(data),
  };
};
