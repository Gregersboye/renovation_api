const express = require('express');

const app = express();

const JsonGenerator = require('./Generators/Json');
const IcalGenerator = require('./Generators/Ical');

const port = 4000;

const Portal = require('./interfaces/AffaldsPortal');


app.get('/address/getAddress/:address', async (req, res) => {
  console.log('address/getAddress was called');
  res.header('Access-Control-Allow-Origin', '*');
  const data = await Portal.getAddress(req.params.address);
  res.send(data);
});

app.get('/address/ical/:addressId/', async (req, res) => {
  console.log(`address/ical was called with id ${req.params.addressId}`);
  const data = await Portal.getServices(req.params.addressId, req.params.raw);

  const cal = new IcalGenerator(data).generate();
  cal.serve(res);
});

app.get('/address/json/:addressId/:raw?', async (req, res) => {
  console.log(`address/json was called with id ${req.params.addressId}`);
  const data = await Portal.getServices(req.params.addressId);

  const returnData = new JsonGenerator(data).generate(req.params.raw);
  res.send(returnData);
});

app.listen(port, () => {
  console.log(`Listening to port ${port}`);
});
