const moment = require('moment');
const Generator = require('./Generator');

class Json extends Generator {
  /**
   *
   * @param {boolean} raw
   */
  generate(raw) {
    return raw ? this.generateRaw() : this.generateFormatted();
  }

  generateFormatted() {
    return this.data.reduce((accumulator, service) => {
      // 'Removing "xxxdag den " from date
      accumulator[service.service] = service.days.map((day) => { return day.slice(-10); });

      return accumulator;
    }, {});
  }

  generateRaw() {
    return this.data
      .reduce((accumulator, service) => {
        const newArray = service.days
          .map((date) => { return { service: service.service, date: date.slice(-10) }; });

        return accumulator.concat(newArray);
      }, [])
      .sort((a, b) => {
        return moment(a.date, 'DD-MM-YYYY').isSameOrBefore(moment(b.date, 'DD-MM-YYYY')) ? -1 : 1;
      });
  }
}

module.exports = Json;
