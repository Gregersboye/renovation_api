const axios = require('axios');
const moment = require('moment');

class Portal {
  static async getServices(addressId) {
    const data = {
      adrid: addressId,
      common: false,
    };

    const response = await axios.post(
      'https://rebild-sb.renoweb.dk/Legacy/JService.asmx/GetAffaldsplanMateriel_mitAffald',
      data,
    )
      .catch((error) => {
        console.log(error.message);
      });

    const services = JSON.parse(response.data.d).list;

    const nextDayQueue = services.reduce((accumulator, service) => {
      const knownService = accumulator
        .some((item) => {
          return item.ordningnavn === service.ordningnavn;
        });

      if (!knownService) {
        accumulator.push(service);
      }

      return accumulator;
    }, [])
      .map(async (service) => {
        const list = await Portal.getId(service.id);

        return Promise.resolve({
          service: service.ordningnavn,
          days: list,
        });
      });

    return Promise.all(nextDayQueue).then((list) => {
      return list;
    });
  }

  /**
   * Fetches the
   * @return {Promise.<void>}
   */
  static async getId(materialId) {
    const data = { materialid: materialId };

    const response = await axios.post(
      'https://rebild.renoweb.dk/Legacy/JService.asmx/GetCalender_mitAffald',
      data,
    ).catch((error) => {
      console.log(error.message);
      console.log('something bad happened');
    });

    return JSON.parse(response.data.d).list;
  }

  static formatDate(date) {
    return moment(date.slice(-10), 'DD-MM-YYYY');
  }

  static async getAddress(address) {
    const data = {
      addresswithmateriel: 3,
      searchterm: address,
    };

    const requestResponse = await axios.post(
      'https://rebild-sb.renoweb.dk/Legacy/JService.asmx/Adresse_SearchByString',
      data,
    ).catch((error) => {
      return {
        statusCode: 500,
        body: error.message,
      };
    });

    return {
      statusCode: 200,
      body: JSON.parse(requestResponse.data.d).list,
    };
  }
}

module.exports = Portal;
